var express = require('express');
var router = express.Router();

const path = require('path');
const cors = require('cors');
const {client} = require('websocket');

const {DateTime} = require('luxon');

const dotenv = require('dotenv');
dotenv.config();

const faunadb = require('faunadb');
var q = faunadb.query;

const FDB_SECRET = process.env.FDB_SECRET;
const FDB_ACCESS_ROLE = 'server';
var FDB_CLIENT = new faunadb.Client({secret: FDB_SECRET});

const {
  Call,
  Paginate,
  Documents,
  Map,
  Collection,
  Lambda,
  Get,
  Var,
  CreateCollection,
  Create,
  Exists,
  Databases,
  Collections,
  Functions,
  Index,
  Intersection,
  Select,
  Indexes,
  Delete,
  Ref,
  Match,
  Update,
  Login,
  Range,
  Time
} = q;

/* GET users listing. */
router.get('/', function (req, res, next) {
  // res.send('Main route for querying vehicle data.');
  console.log('Sending: ', path.join(__dirname, '../public/html/vehicleData.html'));
  res.sendFile(path.join(__dirname, '../public/html/vehicleData.html'));
});

router.get('/getGps', async function (req, res, next) {
  var gps = await getGps();

  res.json({data: gps});
});

router.get('/getVehicles', async function (req, res, next) {
  var vehicles = await getVehicles();
  console.log('/getVehicles vehicles', vehicles);

  res.json({data: vehicles});
});

// Querying functions
async function getVehicles() {
  console.log('getVehicles');
  var faunaGpsResult = await FDB_CLIENT.query(
    Map(Paginate(Documents(Collection('vehicles')), {size: 9999}), Lambda('x', Get(Var('x'))))
  );
  console.log('faunaGpsResult', faunaGpsResult);

  return faunaGpsResult.data;
}

async function getGps() {
  console.log('getGps');
  var faunaGpsResult = await FDB_CLIENT.query(
    Map(Paginate(Documents(Collection('gps')), {size: 9999}), Lambda('x', Get(Var('x'))))
  );
  console.log('faunaGpsResult');

  return faunaGpsResult.data;
}

module.exports = router;
